import { useState } from 'react';
import { differenceInSeconds } from 'date-fns';
import Reading from '../reading';
import css from '../reading/styles.css';


export default ({started}) => {
  const [timer, setTimer] = useState('00:00:00');

  const formatSeconds = (input) => {
    const sec_num = parseInt(input, 10)
    const hours = Math.floor(sec_num / 3600)
    const minutes = Math.floor(sec_num / 60) % 60
    const seconds = sec_num % 60

    return [hours,minutes,seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v,i) => v !== "00" || i > 0)
        .join(":")
  }
  
  setInterval(() => {
    const interval = differenceInSeconds(new Date(), new Date(started));
    setTimer(formatSeconds(interval));
  }, 1000);

  return <Reading value={timer} />
};
