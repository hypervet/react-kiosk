import Reading from '../reading';

export default ({ value }) => <Reading value={value.toFixed(2)} unit="psi" />;
