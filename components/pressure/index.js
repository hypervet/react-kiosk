import { useSocket } from '../../lib/hooks';
import Athmospheres from './athmospheres';
import PSI from './psi';

export default () => {
  const value = useSocket("pressure");
  return (
    <>
      <Athmospheres value={value ? value : 0} />
      <PSI value={value ? value * 14.696 : 0} />
    </>
  );
}
