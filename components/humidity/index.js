import { useSocket } from '../../lib/hooks';
import Reading from '../reading';

export default () => {
  const value = useSocket("humidity")
  return <Reading value={value ? value : 0} unit="%" />
};