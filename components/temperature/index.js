import { useSocket } from '../../lib/hooks';
import Reading from '../reading';

export default () => {
  const value = useSocket("temperature");
  const fahrenheit = value ? (value * (9/5) + 32 ) : 0;
  return <Reading value={fahrenheit.toFixed(1)} unit="F" />
};