import AnalogClock, { Themes } from 'react-analog-clock';

export default () => (
  <AnalogClock theme={Themes.light} width={150} showSmallTicks={false} />
);
