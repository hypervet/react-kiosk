import css from './styles.css';

export default ({ value, unit }) => (
  <section className={css.reading}>
    <p><span className={css.value}>{value}</span>{unit}</p>
  </section>
);
