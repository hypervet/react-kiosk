import { useState } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';
import Router, { withRouter } from 'next/router';
import gql from 'graphql-tag';
import { CHAMBER_ID } from '../config';
import css from '../lib/styles.css';

const CREATE_SESSION = gql`
mutation createSession($chamber: ID!) {
  startSession(input: {chamber: $chamber}) {
    session {
      id
    }
  }
}`;

const ADD_PATIENT_TO_SESSION = gql`
mutation addPatient($patient: ID!, $session: ID!) {
  sessionAddPatient(input: {patient: $patient, session: $session}) {
    session {
      id
    }
  }
}`;

const GET_PATIENTS = gql `
query {
  patients {
    edges {
      node {
        id
        name
        evetpracticeId
        dob
        gender
        breed
        color
      }
    }
  }
}
`;

const Index = () => {
  const [ patient, setPatient ] = useState(null);
  const { loading, data } = useQuery(GET_PATIENTS, {
    pollInterval: 1000 * 60 * 1  // 1 minutes
  });
  const [createSession] = useMutation(CREATE_SESSION, {
    variables: {
      chamber: CHAMBER_ID
    }
  });
  const [addPatient] = useMutation(ADD_PATIENT_TO_SESSION);
  let patientOptions = null;

  if (data && data.patients ) {
    patientOptions = data.patients.edges.map(patient => {
      const { node } = patient;
      return (
      <option key={node.evetpracticeId} value={node.id}>
        {node.name} - {node.dob} - {node.breed} - {node.gender} - {node.color}
      </option>);
    })
  }

  const changeHandler = (event) => setPatient(event.target.value);

  const startSession = async () => {
    const { data } = await createSession();
    await addPatient({
      variables: {
        session: data.startSession.session.id,
        patient: patient
      }
    });
    Router.push('/session/[id]', `/session/${data.startSession.session.id}`);
  }
  return (
    <div className={css.intro}>
      <img src="/img/logo_white.png" /><br/>
      <select onChange={changeHandler}>
        <option>Select patient</option>
        {patientOptions}
      </select>
      <br />
      <p>
        <a className={css.start} onClick={startSession}>Start a session</a>
      </p>
    </div>
  )
};

export default withRouter(Index);