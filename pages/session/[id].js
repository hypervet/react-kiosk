import { useQuery, useMutation } from '@apollo/react-hooks';
import { useRouter } from 'next/router';
import gql from 'graphql-tag';
import Clock from '../../components/clock';
import Athmospheres from '../../components/pressure/athmospheres';
import PSI from '../../components/pressure/psi';
import Timer from '../../components/timer';
import Humidity from '../../components/humidity';
import Temperature from '../../components/temperature';
import { useSocket } from '../../lib/hooks';
import css from '../../lib/styles.css';

const GET_SESSION = gql`
query($id: ID!) {
  session(id: $id) {
    started
    isActive
    willEnd
  }
}
`;

const STOP_SESSION = gql`
mutation($id: ID!) {
  stopSession(id: $id) {
    stopped
  }
}
`

export default () => {
  const router = useRouter();
  const [stopSession] = useMutation(STOP_SESSION)
  const { loading, data } = useQuery(GET_SESSION, {
    variables: { id: router.query.id }
  });
  const pressure = useSocket("pressure");

  const clickStop = async () => {
    console.log(router.query.id);
    const {} = await stopSession({variables: { id: router.query.id }})
    router.push('/');
  }

  if (loading) console.log('loading');
  if(data && data.session) {
    return (
      <main className={css.layout}>
        <header className={css.header}>
          <section>
            <a className={css.stop} onClick={clickStop}>End session</a>
          </section>
          <div>
            <Clock />
          </div>
        </header>
        {/* <aside className={css.athmospheres}>
          <Athmospheres value={pressure ? pressure : 0} />
        </aside>
        <aside className={css.psi}>
          <PSI value={pressure ? pressure * 14.696 : 0} />
        </aside>
        <aside className={css.chart}></aside> */}
        <footer className={css.footer}>
          {/* <Humidity />
          <Temperature /> */}
          <section>&nbsp;</section>
          <section>&nbsp;</section>
          <section>&nbsp;</section>
          <Timer started={data.session.started} />
        </footer>
      </main>
    )
  }
  return <></>
}
