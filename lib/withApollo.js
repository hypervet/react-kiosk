import { withApollo } from 'next-with-apollo';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import { GRAPHQL_URL, USER_TOKEN } from '../config';

const Client = ({ ctx, headers, initialState}) => (
  new ApolloClient({
    uri: GRAPHQL_URL,
    cache: new InMemoryCache().restore(initialState || {}),
    request: operation => operation.setContext(
      {headers: { authorization: `JWT ${USER_TOKEN}` }}
    ),
  })
);

export default withApollo(Client);
