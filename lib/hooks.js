import socketIOClient from 'socket.io-client';
import { useState, useEffect } from 'react';

export const useSocket = (socketName) => {
  const [value, setValue] = useState(null);

  useEffect(() => {
    const socket = socketIOClient(`http://localhost:${process.env.REACT_APP_SERVER}`);
    socket.on(socketName, data => setValue(data));
  })

  return value;
};